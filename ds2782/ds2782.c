#include <unistd.h>
#include <inttypes.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/i2c-dev.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "ds2782.h"

#define BATTERY_ADDRESS		0x34

#define BATTERY_NORATIO_VAL	(1 * 1024)

#define BATTERY_MAP_CURRENT	0x0e
#define BATTERY_MAP_RSGAIN	0x78
#define BATTERY_MAP_RARC	0x06

uint8_t map[0xff] = { 0x00 };

typedef struct {
	uint16_t current;
	uint16_t sr;
	bool reset_ratio;
	bool no_check;
} battery_data_t;

battery_data_t battery_data;

char dev_file[] = "/dev/i2c-1";
int fd;

int16_t battery_current(void);

int i2c_init(void) {
	fd = open(dev_file,O_RDWR);
	printf("Open interface file ( %s )\t\t\t", dev_file);
	if(fd < 0) {
		printf("[ FAIL ]\n");
		return 0;
	}
	printf("[ OK ]\n");

	int dev_addr = 0x34;
	printf("Inital transfer with device\t\t\t\t");
	if(ioctl(fd, I2C_SLAVE, dev_addr) < 0) {
		printf("[ FAIL ]\n");
		return 0;
	}
	printf("[ OK ]\n");

	return fd;
}

int battery_get_map(int fd) {
	uint8_t reg_addr = 0x00;
	if(write(fd, &reg_addr, 1) <= 0) return 1;
	if(read(fd, map, 0xff) <= 0) return 2;

	return 0;
}

uint8_t battery_get_register(uint16_t address, uint8_t *reg) {
	uint8_t reg8;
	if(write(fd, &address, 1) <=0 ) return 1;
	if(read(fd, &reg8, 0x01) <= 0) return 2;
	*(map + address) = reg8;
	if(reg != NULL) *reg = reg8;

	return 0;
}

uint16_t battery_get_register16(uint8_t address, uint16_t *reg) {
	uint8_t reg16[2];
	if(write(fd, &address, 1) <= 0) return 1;
	if(read(fd, reg16, 2) <= 0) return 2;
	*(map + address) = reg16[0];
	*(map + address + 1) = reg16[1];
	if(reg != NULL) *reg = *( (uint16_t *) reg16);

	return 0;
}

uint16_t battery_get_current(void) {
	uint16_t reg;
	int16_t measuring, tmp;
	uint16_t m_delta = 0;

	battery_get_register16(BATTERY_MAP_CURRENT, &reg);
	measuring = battery_current();

	printf("Forward to setting the current values\t.");
	fflush(stdout);

	do {
		sleep(4); // sec
		battery_get_register16(BATTERY_MAP_CURRENT, &reg);
		printf(".");
		fflush(stdout);
		tmp = battery_current();

		m_delta = abs(measuring - tmp);
		measuring = tmp;
	} while(m_delta > 5);

	printf("\n");

	return reg;
}

int16_t battery_current(void) {
	int16_t current = ( ((uint16_t) map[0x0e] ) << 8 ) | map[0x0f];
	current = current * 0.15625;
//	current = current / 10;

	return current;
}

uint16_t battery_calc_ratio(int real, int measurent) {
	float delta = abs(real) * 1.0   / abs(measurent) * 1.0;
	printf("Current ratio value = %f.\n", delta);
	if(delta >= 2.0) return 0;

	return delta * 1024;
}

int battery_set_ratio(uint16_t ratio) {
	uint8_t *raw_ratio;
	raw_ratio  = (uint8_t *) &ratio;

	uint8_t eeprom_rsgain[3] = { BATTERY_MAP_RSGAIN, raw_ratio[1], raw_ratio[0] };
	return write(fd, eeprom_rsgain, 3);
}

int battery_reset_ratio(void) {
	printf("Write RSGAIN (ratio = 1) parameter to EEPROM\t\t");
	if(battery_set_ratio(0x400) == 3) printf("[ OK ]\n");
	else printf("[ FAIL ]\n");
}

void help(char *filename) {
	printf("Use %s [argument]\n", filename);
	printf("\nArguments:\n");
	printf("\t--help\t\t\t-\tprint this message.\n");
	printf("\t--current <value>\t-\tcurrent measure.\n");
	printf("\t--ratio-reset\t\t-\treset current ratio.\n");
	printf("\t--no-check\t\t-\tprint values at this second.\n");
}

int arg(int argc, char *argv[]) {
	if(argc != 1) {
		int i = 1;
		while(i < argc) {
			if(strcmp(argv[i], "--help") == 0) {
				help(argv[0]);
				exit(0);
			} else

			if(strcmp(argv[i], "--current") == 0) {
				// Is there number argument ?l
				if(i + 1 >= argc) 
					goto cli_current_error;

				battery_data.current = atoi(argv[i + 1]);
				i = i + 2;
				if(battery_data.current <= 0) goto cli_current_error; 

				continue;

				cli_current_error:
					printf("Error value of argument current.\n");
					goto cli_error;
			}

			if(strcmp(argv[i], "--ratio-reset") == 0) {
				battery_data.reset_ratio = true;
				i = i + 1;

				continue;
			}

			if(strcmp(argv[i], "--no-check") == 0) {
				battery_data.no_check = true;
				i = i + 1;

				continue;
			}

			goto cli_error;

		}

		goto cli_success;

cli_error:
		printf("Wrong syntax. Use --help option.\n");
		return SM_DS2782_ERROR_SYNTAX;
	}

cli_success:
	return 0;
}

int main(int argc, char *argv[]) {
	memset(&battery_data, 0x00, sizeof(battery_data));

	int ret = arg(argc, argv);
	if(ret != 0) return ret;

	if(i2c_init() == 0) return SM_DS2782_ERROR_DEVICE;

	printf("Reading battery memory\t\t\t\t\t");
	if(battery_get_map(fd) != 0) {
		printf("[ FAIL ]\n");
		return SM_DS2782_ERROR_DEVICE;
	}
	printf("[ OK ]\n");

	if(battery_data.reset_ratio == true) {
		battery_reset_ratio();
	}

	if(battery_data.current > 0) {
		if(battery_data.current < 300) {
			printf("Error. For the ds2782 you need to configure the current was above 300mA!\n\n");
			return SM_DS2782_ERROR_CURRENT_VALUE;
		}
		battery_reset_ratio();
		battery_get_current();

		uint16_t ratio;
		ratio = battery_calc_ratio( battery_data.current, battery_current());
		if(ratio == 0) {
			printf("Error. Ratio can't be more then 1.999.\n\n");
			return SN_DS2782_ERROR_CURRENT_RATIO; 
		}
		printf("Write RSGAIN parameter to EEPROM\t\t\t");
		if(battery_set_ratio(ratio) > 0) printf("[ OK ]\n");
		else printf("[ FAIL ]\n");
	}


	printf("\nInside registers:\n");
	if(!battery_data.no_check)
		battery_get_current(); 

	printf("Current : %d mA\n", battery_current() );

	printf("RSGAIN : 0x%02x 0x%02x\n", map[0x78], map[0x79]);
	printf("RSNSP : 0x%02x\n", map[0x69]);
	printf("Capasity : %u%\n", map[BATTERY_MAP_RARC]);

//	for(int i = 0; i < 16; i++)
//		printf("[0x%02x] : 0x%02x\n", i, map[i]);
}
